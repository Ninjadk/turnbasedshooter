using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class Activateplayer : MonoBehaviour {


	public bool active = false;
	public GameObject playercontroller;
	private FirstPersonController first;


	void Start () {

		first = playercontroller.GetComponent<FirstPersonController>();
		first.enabled = false;
		playercontroller.transform.FindChild ("FirstPersonCharacter").gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void activateplayer () 
	{
		first.enabled = true;
		playercontroller.transform.FindChild ("FirstPersonCharacter").gameObject.SetActive (true);
	}
}
