﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Screen : NetworkBehaviour {

	[SyncVar]private int score = 0;
	public GameObject Sphere;
	//Text text;
	// Use this for initialization
	void Start () {
		
		Instantiate(Sphere,new Vector3(0f,0f,0f), transform.rotation);
		Instantiate(Sphere,new Vector3(30f,0f,0f), transform.rotation);
		Instantiate(Sphere,new Vector3(-30f,0f,0f), transform.rotation);
		//text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		//text.text = "Counter: " + Math.counter;
		gameObject.transform.FindChild ("Score").GetComponent<Text> ().text = "Score:" + score;
		//gameObject.transform.FindChild ("Counter").GetComponent<Text> ().text = "Counter:" + Math.counter;

	}
	void OnTriggerEnter( Collider other)
	{
		if (other.tag == "Treasure") {
			Destroy (other.gameObject);
			Debug.Log ("Score");
			score++;
		}
}
}
