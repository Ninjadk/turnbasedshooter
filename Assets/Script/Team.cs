﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Networking;

public class Team : NetworkBehaviour {


	public List<GameObject> teamlist = new List<GameObject> ();
	public Vector3 playerlocation;

	private int playerid = 0;
	private float distancetravelled = 0;
	private Vector3 lastposition;
	private GameObject firstpersoncontroller;

	// Use this for initialization
	void Awake () {
		
		//firstpersoncontroller = this.gameObject.FindChild ("FirstPersonCharacter");
		foreach (Transform child in transform) 
		{
			teamlist.Add(child.gameObject);


		}
		//playerlocation = teamlist [0].transform.position;
		//lastposition = teamlist [0].transform.position;
		//if (Math.teamturn == 0) {
		//	activate (0);
		//} else { deactivate (0);}
		//deactivate(0);
		activate(0);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!isLocalPlayer) 
		{
			return;
		}

		if (Math.counter > 8) 
		{
			Nextplayer ();
		}
		if (Input.GetKeyDown("space"))
		{
			Nextplayer ();
		}
		distancetravelled += Vector3.Distance (teamlist [playerid].transform.position, lastposition);
		lastposition = teamlist[playerid].transform.position;

		if (distancetravelled > 2.5f) 
		{
			Math.counter++;
			distancetravelled = 0;
		}
	}
	public void Nextplayer()
	{
		deactivate (playerid);
		if (playerid < teamlist.Count-1) {
			playerid++;
		} else {
			Math.turnswitch ();
		}//next player
		activate(playerid);
		lastposition = teamlist [playerid].transform.position;
		Math.counter = 0;

	}

	public void deactivate(int Deactiv)
	{
		teamlist [Deactiv].GetComponent<FirstPersonController> ().enabled = false;
		teamlist[Deactiv].transform.GetChild(0).gameObject.SetActive(false);
	}
	public void activate(int Activ)
	{
		teamlist [Activ].GetComponent<FirstPersonController> ().enabled = true;
		teamlist[Activ].transform.GetChild(0).gameObject.SetActive(true);
	}
	public void Death(GameObject Kill)
	{
		//teamlist.Remove (Kill);

		Debug.Log ("Kill");
	}
}
