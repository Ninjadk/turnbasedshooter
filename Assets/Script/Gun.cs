﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets;

public class Gun : MonoBehaviour {

	public Light lt;
	//private Whenhit whenhit;
	private GameObject Camera;
	public GameObject bullet;
	float distanceTravelled = 0;
	Vector3 lastPosition;

	void Start () {
		lt = GetComponent<Light> ();
		//whenhit = GameObject.FindGameObjectWithTag("Cylinder").GetComponent<Whenhit>();
		Camera = GameObject.FindGameObjectWithTag("MainCamera");
		lastPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		distanceTravelled += Vector3.Distance (transform.position, lastPosition);
		lastPosition = transform.position;
		lt.spotAngle = 2 +  distanceTravelled;
		lt.intensity = 9/(distanceTravelled*0.2f);		 
		transform.rotation = Camera.transform.rotation;
		if (Input.GetMouseButtonDown (0))
		{			
			Instantiate(bullet,transform.position,Quaternion.LookRotation(transform.forward.RandomInCone(Mathf.Deg2Rad*(lt.spotAngle)/2)));
			Math.counter++;
		}
	}

}
